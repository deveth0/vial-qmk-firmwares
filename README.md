# qmk-vial-firmwares

This project automatically builds [vial](https://get.vial.today/) firmwares for keyboards in the [vial-qmk Repository](https://github.com/vial-kb/vial-qmk).
The result is a webpage you can see at [okin.gitlab.io/vial-qmk-firmwares](https://okin.gitlab.io/vial-qmk-firmwares).


The aim of this is to...
* ...make finding a firmware for use with _vial_ easy
* ...make sure the firmware has been build with a recent version of _QMK_
* ...make the used configuration visible

This project has no official affiliation to Vial.
You are using the created firmwares at your own risk.
If you are able to compile your own firmwares it is strongly suggested to do so and use them instead.

## Configuration

Configuration is made through variables inside `.gitlab-ci.yml`.

Additional environment variables can control some of the scripts.
They can be either set directly in `.gitlab-ci.yml` or when manually running a pipeline.

* `KEYMAP` specifies what keymap to search for. Defaults to _vial_ if unset.
* `KEYBOARD` specifies what keyboards to build. There is no default meaning all boards will be prossed. Set a single keyboard like `KEYBOARD=keyhive/ut472` or multiple like `KEYBOARD=ai03/lunar ai03/jp60`.
